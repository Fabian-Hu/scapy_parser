from scapy.all import *
import zlib
import uuid
import re
import sys
import urllib
import json
from datetime import datetime
import subprocess
import paramiko
import sqlite3

nextPacket = 0
savePacket = None 
saveHeader = None
diagnoseCounter = 0

def saveToDatabase(data):
    akt_date = datetime.now().strftime('%d-%m-%Y_%H:%M:%S')
    con = sqlite3.connect('gebauedeDPv.db')
    
    with con:
        cur = con.cursor()
        
        envData = data["EnvironmentSensors"]["devices"][0] 
        cur.execute("INSERT INTO environmentSensor(date,value,rawValue,quantity,unit,pin,scale,offset) VALUES('"+akt_date+"',"+str(envData["value"])+","+str(envData["rawValue"])+",'"+envData["quantity"]+"','"+envData["unit"]+"',"+str(envData["pin"])+","+str(envData["scale"])+","+str(envData["offset"])+")")
        
        stringDataList = data["Strings"]["devices"]
        
        for stringData in stringDataList:       
            insertString = "INSERT INTO string(stringId,date,stringVoltage,stringCurrent,temperature) VALUES("+str(stringData["info"]["id"])+",'"+akt_date+"',"+str(stringData["stringVoltage"])+","+str(stringData["stringCurrent"])+","+str(stringData["temperature"])+")"
            cur.execute(insertString)
            
            for moduleData in stringData["Modules"]["devices"]:
                if moduleData["moduleVoltage"] != 0 and moduleData["moduleTemperature"] != -273.2:
                    insertModule = "INSERT INTO module(moduleSerial,date,stringId,moduleTemperature,moduleVoltage) VALUES('"+moduleData["moduleSerial"]+"','"+akt_date+"',"+str(stringData["info"]["id"])+","+str(moduleData["moduleTemperature"])+","+str(moduleData["moduleVoltage"])+")"
                    cur.execute(insertModule)        
    con.commit()
    
def diagnose():
    global diagnoseCounter
    diagnoseCounter = diagnoseCounter + 1
    print(diagnoseCounter, ": Pakete Empfangen")    

def sendDataOverSSH(json):
    password = "psotZoj7Kp32hrrqL2Ej"
    host = "scl-ifm-min.ad.fh-bielefeld.de"
    username = "gebaeudedpv"
    
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host,username='gebaeudedpv', password='psotZoj7Kp32hrrqL2Ej')
    
    ftp_client=ssh_client.open_sftp()
    akt_date = datetime.now().strftime('%d-%m-%Y_%H:%M:%S')
    file=ftp_client.file('data/data_'+akt_date+'.json', "a", -1)
    file.write(str(json))
    file.flush()
    
    ftp_client.close()
    ssh_client.close()

def dechuckData(http_headers, firstpayload, secondpayload):
    combinedPacket = firstpayload+secondpayload[32:]
    extract_payload(http_headers, combinedPacket)   

def extract_payload(http_headers, payload):
    print(http_headers)
    payload_type = http_headers["Content-Type"].split("/")[1].split(";")[0]
    try:
        if "content-encoding" in http_headers.keys():
            if http_headers["content-encoding"] == "gzip":
                # cut_payload = payload[5:-7] für das GebaeudeD am Campus Minden
                # cut_payload = payload für die Anlange in Ruesselsheim
                cut_payload = payload
                decompressedData = zlib.decompress(cut_payload, 16+zlib.MAX_WBITS)
                percentEncodeData = decompressedData.decode()
                data = urllib.parse.unquote(percentEncodeData)
                data_json = json.loads(data[8:])
                print(data_json)
                #print(data_json["Strings"]["devices"][0])
                #print(data_json["Strings"]["devices"][0]["Modules"]["devices"][0])
                diagnose()
                saveToDatabase(data_json)
                #sendDataOverSSH(data_json)                   
        else:
            file = payload
    except Exception as e:
        print(e)

#Given pcap and output directory, try to extract text payloads from HTTP Responses
#Write text payloads to output directory
def striptxt_pcap(pcap):
    global nextPacket
    global savePacket
    global saveHeader
    
    print(f"[*] Network Capture on pcap file {pcap}. CTRL-C to cancel")
    a = rdpcap(pcap)
    sessions = a.sessions()
    for session in sessions:
        http_payload = b""
        for packet in sessions[session]:
            if nextPacket == 1:
                dechuckData(saveHeader, savePacket, bytes(packet[TCP]))
                nextPacket = 0
            else:
                getDataFromPacket(packet)  
    sys.exit(0)
    
#Given an interface to sniff, try to extract text payloads on the fly and write to file
def striptxt_sniff():
    global nextPacket
    global savePacket
    global saveHeader
    iface = "eth1"
    
    def isChuckedDataCheck(packet):
        try:
            global nextPacket
            global savePacket
            global saveHeader
            if nextPacket == 1:
                dechuckData(saveHeader, savePacket, bytes(packet[TCP]))
                nextPacket = 0
            else:
                getDataFromPacket(packet)        
        except Exception as e:
            print(e)
        
    conf.iface = iface
    try:
        print(f"[*] Network Capture beginns on {iface}. CTRL-C to cancel")
        sniff(iface=conf.iface, prn=isChuckedDataCheck, store=0)
    except KeyboardInterrupt:
        print(f"[*] Exiting")
        sys.exit(0)    
    
def getDataFromPacket(packet):
    global nextPacket
    global savePacket
    global saveHeader
    try:
        if packet[TCP].dport == 80:                   
            payload = bytes(packet[TCP].payload)
            http_header_exists = False
            try:
                http_header = payload[payload.index(b"HTTP/1.1"):payload.index(b"\r\n\r\n")+2]
                if http_header:
                    http_header_exists = True
            except Exception as e:
                pass
            if http_header_exists:
                http_header_raw = payload[:payload.index(b"\r\n\r\n")+2]
                http_header_parsed = dict(re.findall(r"(?P<name>.*?): (?P<value>.*?)\r\n", http_header_raw.decode("utf8")))               
                if "Content-Type" in http_header_parsed.keys():
                    if "application/x-www-form-urlencoded" in http_header_parsed["Content-Type"]:
                        txt_payload = payload[payload.index(b"\r\n\r\n")+4:]
                        if txt_payload:
                            if http_header_parsed["content-encoding"] == "gzip":
                                if len(packet) < 2000:
                                    nextPacket = 1
                                    savePacket = txt_payload
                                    saveHeader = http_header_parsed
                                else:                                 
                                    extract_payload(http_header_parsed, txt_payload)
    except Exception as e:
        pass

#Check cmd line arguments
def start_script(arguments):
    if len(arguments) > 1:
        if arguments[1] == "--inputpcap":
            striptxt_pcap(arguments[2])
        else:
            print("Wrong Argument")
    else:
        striptxt_sniff()
    

#Start the script
start_script(sys.argv)
