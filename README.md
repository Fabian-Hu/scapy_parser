# Forschungsprojekt 

## Beschreibung 

In dem Forschungsprojekt sollen die Daten vom SunsnifferPI auf dem Dach des Gebäude D abgefangen werden. Damit die Daten durch den SmartDataPorter in eine Datenbank gespeichert werden können.

## Verwendete Hardware

1x Raspberry Pi

1x Lan Adapter

1x Lan Kabel

## Installation

### Lan Bridge

In die Konsole eingeben:

```shell
sudo nano /etc/network/interfaces
```

In die Interface Datei schreiben:

```shell
auto br0
iface br0 inet dhcp
bridge_ports eth0 eth1
```

```shell
sudo nano /etc/dhcpcd.conf
```

In die dhcpcd Datei schreiben:

```shell
denyinterfaces eth0 eth1
```

Nun die eigentliche Bridge anlegen.
```shell
sudo apt-get install bridge-utils

sudo brctl addbr br0
sudo brctl addif br0 eth0 eth1
```
Dabei bricht die verbindung ab mit sudo reboot neustarten.

mit ifconfig kann sich die neu erstellte Schnittstelle angezeigt werden.

Danach sollte der SunsnifferPi Internet bekommen und HTTP-Requests versenden.

Hinweis: Nun kann man mit wireshark die ersten Blicke auf die gesendet Daten werfen.

## Scapy Python

Zur Installation

```shell
pip install --pre scapy[basic]
```

ausführen

Scapy Doku:

https://scapy.readthedocs.io/en/latest/introduction.html

Das Repro Klonen

```shell
 git clone https://gitlab.com/Fabian-Hu/scapy_parser.git
```



## NetworkCapture Manuel starten

```shell
cd ~/scapy_parser
sudo python3 scapy_parser.py
```

Hinweis: Sudo ist zwingend notwendig, da sonst der Zugriff auf die Netzwerkschnittstelle verweigert wird.

## SmartMonitoring

### SmartDataPorter

In der Datei GebaeudeDPVParser.java befindet sich der Parser für die PV Daten.

### SmartDataPorterTest

Getestet wird der Parser mit dem Test Projekt zum Porter.

Dabei helfen die Dateien ImporterRessourceTest.java und CollectionTestHelper.java, die jeweils eine Funktion zum Test GebaeudeDPvParser besitzen.

Mit der SmartDataPorterTestCllient.java Klasse kann man die Tests ausführen.

### TODO

Der Datenbankenserver muss auf eine neue Java Version geupdatet werden. Dadurch wurde die Serverseite noch nicht auf Dauer getestet.

## Aktuellen Raspberry Pi erreichen

Fragen sie nach den Zugangsdaten bei:
fhusemann1@fh-bielefeld.de

## TODO

In die ~/bashrc am Ende 

```shell
sudo python3 ~/Desktop/scapy_parser/scapy_parser.py
```

schreiben.

Dieser Schritt wurde bisher noch nicht getan, da auf dem Server erst die Java Version geändert werden muss.  Und ich nicht den Server vollschreiben wollte. 
